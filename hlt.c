#include<stdio.h>


int main() {
   unsigned long a = 5;
   unsigned long b = 10;
   unsigned long sum = 0;
   
   printf("Hello World!\n");

   __asm__("addq %1,%2" : "=r" (sum) : "r" (a), "0" (b));
   printf("a + b = %lu\n", sum);

   __asm__("hlt");
   
   printf("Goodbye World!\n");
}
